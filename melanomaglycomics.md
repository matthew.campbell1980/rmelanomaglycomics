Melanoma Glycomics Dashboard
================

## Summary

MelanomaGlycomicsR provides access to data stored in GlycoStore and
displayed in the Melanoma Glycomics Dashboard. The R package uses dbplyr
to query a local in-memory SQLite database. Example functions are
described below and documented in the man/ folder.

## Dependencies

SQLite is required with the below dependencies:

``` r
library(tidyverse)
library(dplyr)
library(bigrquery)
library(melanomaglycomics)
```

## Getting Started

Recommend loading sqlite from data/, refer to sqlite\_notes.

``` r
# Create a new SQLite connection -----------------------------------------------
con <- connection()
# Get list of cell lines -------------------------------------------------------
cellLines <- getCellLines(con)
```

Now you have a list of cell lines

## Getting Structures for Cell Lines

``` r
# Get structures found in a cell line e.g. HTB-69
cellLine = "HTB-69"
glycans <- getStructuresCell(con, cellLine)
```

## Get Structure Encoding Formats

Information maintained by UniCarbKB-GlyGen project

``` r
formats <- getStructureFormat(con, "G55982TK")
# Relevant fields "wurcs", "iupac", "inchi", "mass", schema description below 
formats["wurcs"]
```

## Get Relative Abundance

``` r
# Get abundance data for a Uoxf named structure for a specified cell line
structure = "A2G2S(3)1"
cellLine = "HTB-69"
structureAbund <- getStructuresAbundanceRelArea(con, cellLine, structure)
```

## Using dbplyr

dbplyr is designed to work with database tables as if they were local
data frames.Use dbply to query the SQLite database.

``` r
# Schema description
?glycomicsdata
# Example query to get cell line, glycan structures, glytoucan identifiers and rel. area from melanoma table (change name is necessary)
table <- tbl(con, "test")
table %>% select("cell", "glycan", "glytoucan", "relarea") %>% filter(glycan == "")  %>% filter(cell == "" ) %>% mutate(relarea = relarea*100)
# Get relative abundance of a glycan
```
