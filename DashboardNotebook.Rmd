---
title: "The Melanoma Glycomics Dashboard - R Notebook"
output: html_notebook
---

MelanomaGlycomicsR provides access to data stored in GlycoStore and displayed in the Melanoma Glycomics Dashboard. The R package uses dbplyr to query a local in-memory SQLite database. Example functions are described below and documented in the man/ folder.

Try executing this chunk by clicking the *Run* button within the chunk or by placing your cursor inside it and pressing *Cmd+Shift+Enter*. 

SQLite is required with the below dependencies:

```{r}
library(tidyverse)
library(dplyr)
library(bigrquery)
library(melanomaglycomics)
```

Recommend loading sqlite from data/, refer to sqlite_notes.

```{r}
con <- connection()
cellLines <- getCellLines(con)
cellLines
```
Now you have a list of cell lines

```{r}
# Get structures found in a cell line e.g. HTB-69
cellLine = "HTB-69"
glycans <- getStructuresCell(con, cellLine)

```

Information maintained by UniCarbKB-GlyGen project

```{r}
formats <- getStructureFormat(con, "G55982TK")
# Relevant fields "wurcs", "iupac", "inchi", "mass", schema description below 
formats["wurcs"]

```

```{r}
# Get abundance data for a Uoxf named structure for a specified cell line
structure = "A2G2S(3)1"
cellLine = "HTB-69"
structureAbund <- getStructuresAbundanceRelArea(con, cellLine, structure)

```

 %>% group_by(cyl) is designed to work with database tables as if they were local data frames.Use dbply to query the SQLite database.

```{r}
# Schema description
?glycomicsdata
# Example query to get cell line, glycan structures, glytoucan identifiers and rel. area from melanoma table (change name is necessary)
table <- tbl(con, "test")
table %>% select("cell", "glycan", "glytoucan", "relarea") %>% filter(glycan == "")  %>% filter(cell == "" ) %>% mutate(relarea = relarea*100)
# Get relative abundance of a glycan

```


When you save the notebook, an HTML file containing the code and output will be saved alongside it (click the *Preview* button or press *Cmd+Shift+K* to preview the HTML file). 

The preview shows you a rendered HTML copy of the contents of the editor. Consequently, unlike *Knit*, *Preview* does not run any R code chunks. Instead, the output of the chunk when it was last run in the editor is displayed.

